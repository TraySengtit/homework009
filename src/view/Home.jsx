import React, { useState } from 'react'
import MyCard from '../component/MyCard';
import { Row, Col, Container } from 'react-bootstrap';
function Home() {
    const [data, setData] = useState([
    {
        id: 1,
        image: 'https://deadline.com/wp-content/uploads/2017/09/your-name-animated-film.jpg',
        title: 'Your name',
        content: 'cartoon Your name'
    },
    {
        id: 2,
        image: 'https://deadline.com/wp-content/uploads/2017/09/your-name-animated-film.jpg',
        title: 'Your name',
        content: 'cartoon Your name'
    },
    {
        id: 3,
        image: 'https://deadline.com/wp-content/uploads/2017/09/your-name-animated-film.jpg',
        title: 'Your name',
        content: 'cartoon Your name'
    },
    {
        id: 4,
        image: 'https://deadline.com/wp-content/uploads/2017/09/your-name-animated-film.jpg',
        title: 'Your name',
        content: 'cartoon Your name'
    },
    {
        id: 5,
        image: 'https://deadline.com/wp-content/uploads/2017/09/your-name-animated-film.jpg',
        title: 'Your name',
        content: 'cartoon Your name'
    },
    {
        id: 6,
        image: 'https://deadline.com/wp-content/uploads/2017/09/your-name-animated-film.jpg',
        title: 'Your name',
        content: 'cartoon Your name'
    },
    {
        id: 7,
        image: 'https://deadline.com/wp-content/uploads/2017/09/your-name-animated-film.jpg',
        title: 'Your name',
        content: 'cartoon Your name'
    }
    ]);
    return (
        <div>
            <Container>
                <Row>
                    {data.map((data) =>
                        <Col md={3}>
                            <MyCard data={data} />
                        </Col>
                    )}
                </Row>
            </Container>
        </div>
    )
}
export default Home
