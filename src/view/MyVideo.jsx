// import React from 'react'
// import {Button, NavLink, ButtonGroup} from 'react-bootstrap'
// import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
// import animation from '../nested/Animation'
// import movie from '../nested/movie'
// function MyVideo() {
//     return (
//         <div>
            // <h1>Video</h1>
            // <ButtonGroup aria-label="Basic example">
            //     <Button as={NavLink} to='/movie' variant="secondary">Movies</Button>
            //     <Button as={NavLink} to='/animation' variant="secondary">Animations</Button>
            // </ButtonGroup>
//             <Router>
//                 <Switch>
//                     <Route path='/animation' component={animation} />
//                     <Route path='/movie' component={movie} />
//                 </Switch>
//             </Router>
//         </div>
//     )
// }

// export default MyVideo

import React from "react";
import {ButtonGroup, Button} from 'react-bootstrap';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  NavLink,
  useParams,
  useRouteMatch
} from "react-router-dom";


export default function MyVideo() {
  return (
    <Router>
      <div>
        <h1>Video</h1>
            <ButtonGroup aria-label="Basic example">
                <Button as={Link} to='/movie' variant="secondary">Movies</Button>
                <Button as={Link} to='/animations' variant="secondary">Animations</Button>
            </ButtonGroup>

        <Switch>
          <Route exact path="/movie">
            <Movie />
          </Route>
          <Route path="/animations">
            <Animations />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

function Movie() {
    let { path, url } = useRouteMatch();
    return (
        <div>
            <h2>Moive Category</h2>
            <Button as={NavLink} to={`${url}/adventures`} variant="secondary">Adventure</Button>
            <Button as={NavLink} to={`${url}/crimes`} variant="secondary">Crime</Button>
            <Button as={NavLink} to={`${url}/actions`} variant="secondary">Action</Button>
            <Button as={NavLink} to={`${url}/romances`} variant="secondary">Romance</Button>
            <Button as={NavLink} to={`${url}/comedys`} variant="secondary">Comedy</Button>

            <Switch>
            <Route exact path={path}>
                <h3>Please Choose Category.</h3>
            </Route>
            <Route path={`${path}/:m_Id`}>
                <Movie_cate />
            </Route>
            </Switch>
        </div>
    );
}
function Movie_cate() {

    let { m_Id } = useParams();

    return (
        <div>
            <h3>Please Choose Category: {m_Id}</h3>
        </div>
    );
}

function Animations() {
    let { path, url } = useRouteMatch();

    return (
    <div>
        <h2>Animation Category</h2>
            <Button as={Link} to={`${url}/adventure`} variant="secondary">Adventure</Button>
            <Button as={Link} to={`${url}/crime`} variant="secondary">Crime</Button>
            <Button as={Link} to={`${url}/action`} variant="secondary">Action</Button>
            <Button as={Link} to={`${url}/romance`} variant="secondary">Romance</Button>
            <Button as={Link} to={`${url}/comedy`} variant="secondary">Comedy</Button>

        <Switch>
        <Route exact path={path}>
            <h3>Please Choose Category.</h3>
        </Route>
        <Route path={`${path}/:ani_Id`}>
            <Ani_cate />
        </Route>
        </Switch>
    </div>
    );
}
function Ani_cate() {

    let { ani_Id } = useParams();

    return (
        <div>
            <h3>Please Choose Category: {ani_Id}</h3>
        </div>
    );
}
