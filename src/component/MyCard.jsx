import React from 'react'
import {Card, Button, Container} from 'react-bootstrap'

function MyCard({data}) {
    return (
        
        <div>
            <Card className="my-2">
                <Card.Img variant="top" src={data.image} />
                <Card.Body>
                    <Card.Title>{data.title}</Card.Title>
                    <Card.Text>
                        {data.content}
                    </Card.Text>
                    <Button variant="light" size="sm">Read</Button>
                </Card.Body>
            </Card>
        </div>
        
    )
}

export default MyCard
