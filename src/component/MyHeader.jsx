import React from 'react'
import {Container, Nav, Navbar, Form, Button, FormControl} from 'react-bootstrap'
import { NavLink } from 'react-router-dom'
function MyHeader() {
    return (
        <div>
            <Navbar bg="light" expand="lg">
                <Container>
                <Navbar.Brand href="#">React-Router</Navbar.Brand>
                <Navbar.Toggle aria-controls="navbarScroll" />
                <Navbar.Collapse id="navbarScroll">
                    <Nav
                        className="mr-auto my-2 my-lg-0"
                        style={{ maxHeight: '100px' }}
                        navbarScroll
                    >
                        <Nav.Link as={NavLink}to='/'>Home</Nav.Link>
                        <Nav.Link as={NavLink} to='/myvideo'>Video</Nav.Link>
                        <Nav.Link as={NavLink} to='/account'>Account</Nav.Link>
                        <Nav.Link as={NavLink} to='/mywelcome'>Welcome</Nav.Link>
                        <Nav.Link as={NavLink} to='/auth'>Auth</Nav.Link>
                    </Nav>
                    <Form className="d-flex">
                        <FormControl
                            type="search"
                            placeholder="Search"
                            className="mx-2"
                            aria-label="Search"
                        />
                        <Button variant="outline-success">Search</Button>
                    </Form>
                </Navbar.Collapse>
                </Container>
            </Navbar>
        </div>
    )
}

export default MyHeader
