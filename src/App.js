import React from 'react'
import MyCard from '../src/component/MyCard';
import MyHeader from '../src/component/MyHeader';
import { Row, Col, Container, Accordion } from 'react-bootstrap';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import Home from './view/Home';
import MyVideo from './view/MyVideo';
import Account from './view/Account';
import MyWelcome from './view/MyWelcome';
function App() {
  return (
    <>

      <Router>
        <MyHeader />
        <Container>
          <Switch>
            <Route exact path='/' component={Home}></Route>
            <Route path='/myvideo' component={MyVideo} />
            <Route path='/account' component={Account} />
            <Route path='/mywelcome' component={MyWelcome} />
            <Route path='/auth' component={MyVideo} />
          </Switch>
        </Container>
      </Router>

    </>
  );
}

export default App;

